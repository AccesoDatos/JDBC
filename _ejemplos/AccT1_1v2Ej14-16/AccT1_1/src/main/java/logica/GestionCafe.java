package logica;

import java.util.List;

import dao.CafeDAO;
import dao.FactoriaDAO;
import modelo.Cafe;
import modelo.Error;

public class GestionCafe {
	public List<Cafe> coleccionCafe() throws Error{
		List<Cafe> cafes = null;
		CafeDAO cd = FactoriaDAO.getInstance().getCafeDAO();
		cafes = cd.verTabla();
		return cafes;
	}
}
