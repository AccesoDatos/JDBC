package modelo;

public class Error extends Exception{

	private static final long serialVersionUID = 6586175136912353223L;

	public Error(String mensaje_error) {
		super(mensaje_error);
	}
}
