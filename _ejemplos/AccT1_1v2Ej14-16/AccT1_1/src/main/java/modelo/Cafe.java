package modelo;

public class Cafe {
	private String caf_nombre;
	private float precio;
	private int ventas;
	private float total;
	private int prov;
	
	public Cafe(String caf_nombre, float precio, int ventas, float total, int prov) {
		super();
		this.caf_nombre = caf_nombre;
		this.precio = precio;
		this.ventas = ventas;
		this.total = total;
		this.prov = prov;
	}
	public String getCaf_nombre() {
		return caf_nombre;
	}
	public void setCaf_nombre(String caf_nombre) {
		this.caf_nombre = caf_nombre;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public int getVentas() {
		return ventas;
	}
	public void setVentas(int ventas) {
		this.ventas = ventas;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public int getProv() {
		return prov;
	}
	public void setProv(int prov) {
		this.prov = prov;
	}
	@Override
	public String toString() {
		return "Cafe >> caf_nombre=" + caf_nombre + " precio=" + precio + 
				" ventas=" + ventas + " total=" + total
				+ " prov=" + prov;
	}
	
}
