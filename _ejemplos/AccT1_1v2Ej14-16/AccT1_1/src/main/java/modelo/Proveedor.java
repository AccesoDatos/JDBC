package modelo;

public class Proveedor {
	private int prov_id;
	private String prov_nombre;
	private String calle;
	private String ciudad;
	private String pais;
	private String cp;

	public Proveedor(int prov_id, String prov_nombre, String calle, String ciudad, String pais, String cp) {
		super();
		this.prov_id = prov_id;
		this.prov_nombre = prov_nombre;
		this.calle = calle;
		this.ciudad = ciudad;
		this.pais = pais;
		this.cp = cp;
	}

	public int getProv_id() {
		return prov_id;
	}
	public void setProv_id(int prov_id) {
		this.prov_id = prov_id;
	}
	public String getProv_nombre() {
		return prov_nombre;
	}
	public void setProv_nombre(String prov_nombre) {
		this.prov_nombre = prov_nombre;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	@Override
	public String toString() {
		return "Proveedor>> prov_id=" + prov_id + " prov_nombre=" + prov_nombre +
				", calle=" + calle + " ciudad="
				+ ciudad + " pais=" + pais + " cp=" + cp + "]";
	}
}
