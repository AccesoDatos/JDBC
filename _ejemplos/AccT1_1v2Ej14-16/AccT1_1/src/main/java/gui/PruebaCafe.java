package gui;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import dao.CafeDAO;
import dao.FactoriaDAO;
import dao.ProveedorDAO;
import logica.GestionCafe;
import modelo.Cafe;
import modelo.Error;
import modelo.Proveedor;

public class PruebaCafe {
	public static void main(String[] args) {
		int opc = 0;
		try {
			CafeDAO c = FactoriaDAO.getInstance().getCafeDAO();
			ProveedorDAO p = FactoriaDAO.getInstance().getProveedorDAO();
			Scanner sc = new Scanner(System.in);
			while (opc!=13){
				System.out.println("¿Que opcion desea? \n 1-VerTabla 2-BuscarCafe 3-BuscarProv 4-InsertarCafe \n"
						+ "5-BorrarCafe 6-InsertarCafe 7-Modificar€ 8-ActualizarVenta 9-Transferencia\n"
						+ " 10-MostrarBorrar 11-Crear un proveedor"
						+ "\n 12-Buscar prov 13-Dato prov-caf 14-Salir" );
				switch(Integer.parseInt(new Scanner (System.in).nextLine())){
					case 1:
						mostrar(new GestionCafe().coleccionCafe());
						break;
					case 2:
						System.out.println("Identificador del cafe: ");
						c.buscarCafe(new Scanner (System.in).nextLine());
						break;
					case 3:
						System.out.println("Identificador del proveedor: ");
						int b = Integer.parseInt( new Scanner (System.in).nextLine());
						c.buscarProveedor(b);
						break;
					case 4:
						System.out.println("Nombre del cafe? "+"Proveedor id "+ "El precio "+ "Las ventas "+"El total ");
						c.insertarCafe(new Scanner (System.in).nextLine(), Integer.parseInt(new Scanner (System.in).nextLine()),Float.parseFloat(new Scanner (System.in).nextLine()), Integer.parseInt(new Scanner (System.in).nextLine()) , Integer.parseInt(new Scanner (System.in).nextLine()));
						break;
					case 5:
						System.out.println("NOmbre del cafe que desea borrar: ");
						c.borrarCafe(new Scanner (System.in).nextLine());
						break;
					case 6:
						c.insertarCafeS("Colombianism", 101, 26.99f, 175, 175);
						c.insertarCafeS("La Estrella Star", 150, new Float(1.99), 700, 800);
						break;
					case 7:
						c.modificarPrecio(new Float(1.5));	
					case 8:
						HashMap<String, Integer> ventas = new HashMap<String, Integer>();
						ventas.put("Colombian", 110);
						ventas.put("Colombian_Decaf ", 150);
						c.actualizarVentas(ventas);
						break;
					case 9:
						System.out.println("Cafe primero enter Cafe segundo");
						c.transferencia(new Scanner (System.in).nextLine(), new Scanner (System.in).nextLine());
						break;
					case 10: 
						System.out.println("Introduzca el numero de la fila");
						c.mostrarBorrar(Integer.parseInt(new Scanner (System.in).nextLine()));
						break;
					case 11:
						System.out.println("Introduzca los siguientes datos: "
								+ "id, nombre, calle, ciudad, pais, cp");
						Proveedor miprv = new Proveedor(112,"S.Alice","c/Jimena 20","Madrid", "SP","21521");
						p.insertarPrv(miprv);
						break;
					case 12:
						System.out.println("BUsCAR PROVEEDOR");
						System.out.println(p.buscarPrv(112));
						
						break;
					case 13:
						System.out.println(">>>>SACANDO CAFES DEL PROVEEDOR");
						System.out.println(">>>>Ultimo ejercicio de esta seccion "
								+ "Mostrando tambien datos de este proveedor 101");
						c.provCafes(101);
						System.out.println(p.buscarPrv(101));
						break;
					case 14:
						c.exit();
						sc.close();
						break;
				}
			}
		} catch (Error e) {
			System.err.println(e.getMessage());
		}
		
	}
	//CAMBIADO
	public static void mostrar(List<Cafe> list){
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).toString());
		}
		
	}

}
