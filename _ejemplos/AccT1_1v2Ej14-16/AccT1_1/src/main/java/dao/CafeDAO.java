package dao;
import java.util.HashMap;
import java.util.List;

import modelo.Cafe;
import modelo.Error;

public interface CafeDAO {
	public void closeState() throws Error;
	public List<Cafe> verTabla();
	public void buscarCafe(String nombrecafe) throws Error;
	public void buscarProveedor(int proveedor) throws Error;
	public void insertarCafe (String nombrecafe, int provid, float precio, int ventas,int total) throws Error;
	public void borrarCafe (String nombrecafe) throws Error;
	public void insertarCafeS(String nombrecafe, int provid, float precio, int ventas,int total) throws Error;
	public void modificarPrecio (float porcentaje) throws Error;
	public void actualizarVentas(HashMap<String, Integer> ventas) throws Error;
	public void transferencia (String cafe1, String cafe2);
	public void mostrarBorrar(int pos) throws Error;
	public void provCafes(int prov_id) throws Error;
	public void exit();
}
