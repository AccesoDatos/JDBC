package dao;
import modelo.Error;
import modelo.Proveedor;

public interface ProveedorDAO {
	public void insertarPrv(Proveedor proveedor)  throws Error;
	public Proveedor buscarPrv(int idProveedor) throws Error;
	
}
