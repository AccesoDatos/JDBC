package dao;

import modelo.Error;

public class FactoriaDAO {
	private static FactoriaDAO instance;
	private static final String cafeDAO="JDBCCafeDAO";
	private static final String proveedorDAO="JDBCProveedorDAO";
	private FactoriaDAO(){}
	public static FactoriaDAO getInstance(){
		if(instance==null){
			instance = new FactoriaDAO();
		}
		return instance;
	}
	public CafeDAO getCafeDAO() throws Error{
		CafeDAO dao = null;
		if(cafeDAO.equals("JDBCCafeDAO")){
			dao = new JDBCCafeDAO();
		}
		return dao;
	}
	public ProveedorDAO getProveedorDAO() throws Error{
		ProveedorDAO dao = null;
		if(proveedorDAO.equals("JDBCProveedorDAO")){
			dao = new JDBCProveedorDAO();
		}
		return dao;
	}
}
