package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Map;

import modelo.Cafe;
import modelo.Error;
import modelo.Proveedor;

// IMplementar todos los metodos para hacer funcionar la tecnologia dao
public class JDBCCafeDAO implements CafeDAO {
	private Connection conJava;
	private ResultSet r;
	private Statement s;
	private PreparedStatement ps1;
	private PreparedStatement ps2;
	private Cafe c;
	
	public JDBCCafeDAO() throws Error {
		try {
			this.conJava = (Connection) new Conexion().getConexion();
			r=null;s=null;ps1=null;ps2=null;
		} catch (InvalidPropertiesFormatException e) {System.err.println("Error de formato");
		} catch (IOException e) {System.err.println(e.getMessage());throw new Error("Acceso a propiedades error");
		}		
	}
	// CERRAR SENTENCIAS
	public void closeState(){
		try {
			if(r!=null){r.close();}
			if(s!=null){s.close();}
			if(ps1!=null){	ps1.close();}
			if(ps2!=null){	ps2.close();}
		} catch (SQLException e) {Conexion.printSQLException(e);}
	}
	//CERRAR CONEXION
	public void exit() {
		if(conJava!=null){Conexion.exitConexion(conJava);}
	}

//	VISTA BASE DE DATOS 
	public static final String SELECT_CAFE = "select CAF_NOMBRE, PROV_ID, PRECIO, VENTAS, TOTAL from CAFES";
	public List<Cafe> verTabla(){
		List<Cafe> lc = null;//
		s=null;r=null;
		System.out.println("MOSTRANDO CAFE . . . ");
		try {
			lc = new ArrayList<Cafe>();//
			s = (Statement) conJava.createStatement();
			r = s.executeQuery(SELECT_CAFE);
			while (r.next()){
				lc.add(new Cafe(r.getString("caf_nombre"), r.getFloat("precio"), r.getInt("ventas"), r.getFloat("total"), r.getInt("prov_id")));//
			}System.out.println();
		} catch (SQLException e) {Conexion.printSQLException(e);}
		finally{closeState();}
		return lc;
	}

	//VISTA CAFES METODO RETURN STRING
	private String vistaCafe(ResultSet r,boolean nombre, boolean precio, boolean ventas, boolean total, boolean proveedor) throws SQLException{
		StringBuffer sb = new StringBuffer("");
		if (nombre==true)sb.append("Nombre Cafe >>"+r.getString("CAF_NOMBRE"));
		if(precio==true)sb.append(" - Precio: "+ r.getFloat("PRECIO"));
		if(ventas==true)sb.append(" - Ventas: "+r.getInt("VENTAS"));
		if(total==true)sb.append(" - Total: "+r.getInt("TOTAL"));
		if(proveedor==true)sb.append(" - Proveedor: "+  r.getInt("PROV_ID"));
		return sb.toString();
	}	
//	BUSCAR FK LAS DOS TABLAS
	public static final String SEARCH_CAFE = "select * from CAFES WHERE CAF_NOMBRE= ?";
	public void buscarCafe(String nombrecafe) throws Error{
		ps1=null;
		System.out.println("BUSCANDO CAFE PK : "+nombrecafe);
		try {
			ps1=(PreparedStatement) conJava.prepareStatement(SEARCH_CAFE);
			ps1.setString(1, nombrecafe);
			r=ps1.executeQuery();
			while (r.next()){
				Cafe c = new Cafe(r.getString("caf_nombre"), r.getFloat("precio"), r.getInt("ventas"), r.getFloat("total"), r.getInt("prov_id"));//
				System.out.println(c.toString());;//
			}
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error ("Ocurrio un error al acceder a los datos");
		}finally{			closeState();		}
	}
	
//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------EJERCICIO 1-------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------
	public static final String SEARCH_CAFE_prv = "select c.caf_nombre,c.precio,c.ventas, c.total,p.PROV_ID,p.PROV_NOMBRE, p.CALLE, p.CIUDAD, p.PAIS, p.CP  from cafes c inner join proveedores p  on c.PROV_ID=p.PROV_ID where c.PROV_ID=?;";
	public void buscarProveedor(int proveedor) throws Error{
		ps1=null;
		System.out.println("MOSTRANDO CAFES-PROVEEDOR CON ESE PROVEEDOR: "+proveedor);
		try {
			ps1 = (PreparedStatement) conJava.prepareStatement(SEARCH_CAFE_prv);
			ps1.setInt(1, proveedor);
			r = ps1.executeQuery();
			while (r.next()){
				System.out.println(vistaCafe(r,true, true, true, true, false)+"\n --- PROVEEDOR "+vistaProveedor(r, true, true, true, true, true, true));
			}
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error ("Ocurrio un error al acceder a los datos");
		}finally{			closeState();		}
	}
	//VISTA PROVEEDOR RETURN STRING
	private String vistaProveedor(ResultSet r,boolean prov_id, boolean prov_nombre, boolean calle, boolean ciudad, boolean pais,boolean cp) throws SQLException{
		StringBuffer sb = new StringBuffer("");
		if (prov_id==true)sb.append("Id Proveedor >>"+r.getString("prov_id"));
		if(prov_nombre==true)sb.append(" - Nombre: "+ r.getString("prov_nombre"));
		if(calle==true)sb.append(" - Calle: "+r.getString("calle"));
		if(ciudad==true)sb.append(" - Ciudad: "+  r.getString("ciudad"));
		if(pais==true)sb.append(" - Pais: "+  r.getString("pais"));
		if(cp==true)sb.append(" - Cod.Postal: "+  r.getString("cp"));
		return sb.toString();
	}
	//-------------------------------------------------------------------------------------------------------------------------------
													//PREPARED STATEMENT - MAS SEGURO
	public static final String INSERT_CAFE = "insert into CAFES values (?,?,?,?,?)";
	public void insertarCafe (String nombrecafe, int provid, float precio, int ventas,int total) throws Error{
		ps1 = null;
		System.out.println("INSERTANDO CAFE PK : "+nombrecafe);
		try {
			ps1=(PreparedStatement) conJava.prepareStatement(INSERT_CAFE);
			ps1.setString(1, nombrecafe);ps1.setInt(2,provid);ps1.setFloat(3, precio);ps1.setInt(4, ventas);ps1.setInt(5, total);
			ps1.executeUpdate();
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error ("Ocurrio un error al acceder a los datos");
		}finally{			closeState();		}
	}
	
	public static final String DELETE_CAFE = "delete from CAFES WHERE CAF_NOMBRE = ?";
		public void borrarCafe (String nombrecafe) throws Error{
			ps1 = null;	
			System.out.println("BORRANDO CAFE PK : "+nombrecafe);
			try {
				ps1=(PreparedStatement) conJava.prepareStatement(DELETE_CAFE);
				ps1.setString(1, nombrecafe);
				ps1.executeUpdate();
				
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				throw new Error ("Ocurrio un error al acceder a los datos");
			}finally{			closeState();		}
		}
		//-------------------------------------------------------------------------------------------------------------------------------
													//STATEMENT - MENOS SEGURO

		public void insertarCafeS(String nombrecafe, int provid, float precio, int ventas,int total) throws Error{
			s=null;r=null;
			try {
				s=(Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(SELECT_CAFE);
				r.moveToInsertRow();
				r.updateString("CAF_NOMBRE", nombrecafe);
				r.updateInt("PROV_ID", provid);
				r.updateFloat("PRECIO", precio);
				r.updateInt("VENTAS", ventas);
				r.updateInt("TOTAL", total);
				r.insertRow();
				r.beforeFirst();
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				throw new Error ("Ocurrio un error al acceder a los datos");
			}finally{			closeState();		}
		}
		
		public void modificarPrecio (float porcentaje) throws Error{
			closeState();
			System.out.println("MODIFICANDO PRECIO");
			try {
				s=(Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(SELECT_CAFE);
				while (r.next()){
					r.updateFloat("PRECIO", r.getFloat("PRECIO")*porcentaje);
					r.updateRow();
				}
				r.beforeFirst();
				
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				throw new Error ("Ocurrio un error al acceder a los datos");
			}finally{			closeState();		}
		}
		//-------------------------------------------------------------------------------------------------------------------------------
													//PREPARED STATEMENT - MAS SEGURO
		public static final String UPDATE_CAFE_ventas = "update CAFES set VENTAS = ? where CAF_NOMBRE = ?";
		public static final String UPDATE_CAFE_total = "update CAFES set TOTAL = TOTAL + ? where CAF_NOMBRE = ?";
		public void actualizarVentas(HashMap<String, Integer> ventas) throws Error {
			ps1 = null; ps2 = null;
			System.out.println("\n ACTUALIZANDO LAS VENTAS . . .");
			try {// Desactivamos el autocommit. Actualizamoms con ps1 el cafe y el costo y ps2 la suma
				conJava.setAutoCommit(false);
				ps1 = (PreparedStatement) conJava.prepareStatement(UPDATE_CAFE_ventas);
				ps2 = (PreparedStatement) conJava.prepareStatement(UPDATE_CAFE_total);

				for (Map.Entry<String, Integer> e : ventas.entrySet()) {
					ps1.setInt(1, e.getValue().intValue());
					ps1.setString(2, e.getKey());
					ps1.executeUpdate();
					
					ps2.setInt(1, e.getValue().intValue());
					ps2.setString(2, e.getKey());
					ps2.executeUpdate();
					// Hacemos un commit de forma explicita cuando hemos ejecutado
					// todas las operaciones de la transaccion
					conJava.commit();
				}
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				if (conJava != null) {
					try {//Si falla todos los datos de ps se deshacen
						System.err.println("Roll back de la transaccion");
						conJava.rollback();
					} catch (SQLException excep) {
						Conexion.printSQLException(e);
					}
					throw new java.lang.Error(
							"Ocurrio un error al acceder a los datos");
					}
			} finally {
				closeState();
				try {// Volvemos a habilitar el autocommit
					conJava.setAutoCommit(true);
				} catch (SQLException ex) {
					Conexion.printSQLException(ex);
				}
			}
		}
		//-------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------EJERCICIO 12  -------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------------------------
		public static final String select_CAFE_ventas = "select ventas from cafes where CAF_NOMBRE = ?";
		public void transferencia (String cafe1, String cafe2){
			System.out.println("Haciendo TRANSAFERENCIAION entre las ventas de primer cafe al segundo");
			try {// Desactivamos el autocommit. Actualizamoms con ps1 el cafe y el costo y ps2 la suma
				conJava.setAutoCommit(false);
				//Operacion 1: cogemos las ventas 
				int ventas1 = 0, ventas2 = 0;
				ps1 = (PreparedStatement) conJava.prepareStatement(select_CAFE_ventas);
				ps1.setString(1, cafe1);
				r=ps1.executeQuery();
				while (r.next()){
					ventas1 = r.getInt("ventas");
				}
				//Metemos el cafe 2
				ps1.setString(1, cafe2);
				r=ps1.executeQuery();
				while (r.next()){
					ventas2 = r.getInt("ventas");
				}
				//Operacion 2: asentar el total de las ventas en el cafe 2
				ps2 = (PreparedStatement) conJava.prepareStatement(UPDATE_CAFE_ventas);
				ps2.setInt(1, ventas1+ventas2); // COGER LAS VENTAS DE ESTE CAFE Y PONER LAS OTRAS A 0
				ps2.setString(2, cafe2);
				ps2.executeUpdate();
				//Operacion 3: poner el anterior a 0
				ps2.setInt(1, 0); // COGER LAS VENTAS DE ESTE CAFE Y PONER LAS OTRAS A 0
				ps2.setString(2, cafe1);
				ps2.executeUpdate();
				conJava.commit();
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				if (conJava != null) {
					try {//Si falla todos los datos de ps se deshacen
						System.err.println("Roll back de la transaccion");
						conJava.rollback();
					} catch (SQLException excep) {
						Conexion.printSQLException(e);
					}
					throw new java.lang.Error(
							"Ocurrio un error al acceder a los datos");
					}
			} finally {
				closeState();
				try {// Volvemos a habilitar el autocommit
					conJava.setAutoCommit(true);
				} catch (SQLException ex) {
					Conexion.printSQLException(ex);
				}
			}
		}

		public void mostrarBorrar(int pos) throws Error {
			Statement stmt = null;r = null;
			try {
				stmt = (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				r = stmt.executeQuery(SELECT_CAFE);

				/*
				 * Para insertar una fila a trav�s del RS, hay que posicionar el
				 * cursor en una fila especial llamada "insert Row" en la que puedo
				 * dar valores a los campos
				 */
				r.absolute(pos);
				System.out.println("\nEn mostrar y borrar:\n");
				System.out.println(vistaCafe(r,true, true, true, true, true));
				r.deleteRow();

			} catch (SQLException sqle) {
				// En una aplicaci�n real, escribo en el log y delego
				Conexion.printSQLException(sqle);
				throw new Error(
						"Ocurrio un error al acceder a los datos");

			} finally {
				closeState();
			}
		}
		
		public static final String PRV_CF="select * from cafes where prov_id=?";
		public void provCafes(int prov_id) throws Error{
			ps1=null;r=null; c = null;
			try{
				ps1 = conJava.prepareStatement(PRV_CF);
				ps1.setInt(1, prov_id);
				r=ps1.executeQuery();
				if(r.first()){
					r.beforeFirst();
					while(r.next()){
						System.out.println(vistaCafe(r,true, true, true, true, true));
					}
				}else{
					System.out.println("No existe tal identificador");
				}
			}catch(SQLException e){
				Conexion.printSQLException(e);
				throw new Error("Error al acceder a los datos");
			}finally{
				closeState();
			}
		}
}
