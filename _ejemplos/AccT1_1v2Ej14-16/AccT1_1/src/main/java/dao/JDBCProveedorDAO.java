package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InvalidPropertiesFormatException;

import modelo.Error;
import modelo.Proveedor;

public class JDBCProveedorDAO implements ProveedorDAO{
	private Connection conJava;
	private ResultSet r;
	private Statement s;
	private PreparedStatement p;
	private PreparedStatement p2;
	private Proveedor prv;
	
	public JDBCProveedorDAO () throws Error{
		try {
			this.conJava = (Connection) new Conexion().getConexion();
			r=null;s=null;p=null;p2=null;prv = null;
		} catch (InvalidPropertiesFormatException e) {System.err.println("Error de formato");
		} catch (IOException e) {System.err.println(e.getMessage());throw new Error("Acceso a propiedades error");
		}
	}
	// CERRAR SENTENCIAS
	public void closeState()  throws Error{
		try {
			if(r!=null){r.close();}
			if(s!=null){s.close();}
			if(p!=null){p.close();}
			if(p2!=null){p2.close();}
		} catch (SQLException e) {Conexion.printSQLException(e);}
	}
	public static final String INSERTAR_PRV = "insert into proveedores values (?,?,?,?,?,?);";
	public void insertarPrv(Proveedor prv) throws Error{
		p=null;this.prv = null;
		System.out.println("INSERTANDO PROVEEDOR EN LA BASE DE DATOS ");
		try {
			p= conJava.prepareStatement(INSERTAR_PRV);
			p.setInt(1, prv.getProv_id());
			p.setString(2, prv.getProv_nombre());
			p.setString(3, prv.getCalle());
			p.setString(4, prv.getCiudad());
			p.setString(5, prv.getPais());
			p.setString(6, prv.getCp());
			p.executeUpdate();
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error ("Ocurrio un error al acceder a los datos");
		}finally{
			closeState();		}
	}
	public static final String BUSCAR_PRV="select * from proveedores where prov_id=?";
	public Proveedor buscarPrv(int idProveedor) throws Error {
		p=null;r=null;this.prv = null;
		System.out.println("BUSCANDO PROVEEDOR PK : "+idProveedor);
		try {
			p= conJava.prepareStatement(BUSCAR_PRV);
			p.setInt(1, idProveedor);
			r=p.executeQuery();
			if(r.first()){
				this.prv = new Proveedor(r.getInt("prov_id"), r.getString("prov_nombre"),
						r.getString("calle"), r.getString("ciudad"), r.getString("ciudad")
						, r.getString("ciudad"));
			}else
				System.out.println("El proveedor proporcionado no existe");
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error ("Ocurrio un error al acceder a los datos");
		}finally{			closeState();		}
		return this.prv;
	}

}
