package dao;
import model.Error;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.commons.dbcp2.BasicDataSource;

public class LibroDao {
	private BasicDataSource pool;
	private Connection conJava;
	private java.sql.Statement s;
	private PreparedStatement p;
	private ResultSet r;

	public LibroDao() throws Error {
		try {
			this.pool = new ConexionPool().getPool();
			s=null;p=null;r=null;
		}  catch (SQLException ex) {
			ConexionPool.printSQLException(ex);
			throw new Error("Ocurrio un error al acceder a la base de datos");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	public void cerrarPool(){
		ConexionPool.cerrarPool(pool);
	}
	public void cerrarR(){
		try {
			if(s!=null)s.close();
			if(p!=null)p.close();
			if(conJava!=null)conJava.close();
		} catch (SQLException e) {
			//Aqui no delega porque es error al liberarlos
		}
	}
	//METODO GENERAL - MODIFICADO PARA EL EJERCICIO 8
		public String stringResultado(ResultSet r,boolean isbn,boolean titulo,boolean autor,
				boolean editorial,boolean paginas,boolean copias, boolean precio ) throws SQLException{
			StringBuffer sb = new StringBuffer ("");
			if (isbn!=false) sb.append("isbn:"+r.getInt("isbn")+" ");
			if (titulo!=false) sb.append("title:"+r.getString("titulo")+" ");
			if (autor!=false) sb.append("autor:"+r.getString("autor")+" ");
			if (editorial!=false) sb.append("editorial:"+r.getString("editorial")+" ");
			if (paginas!=false) sb.append("pages:"+r.getInt("paginas")+" ");
			if (copias!=false) sb.append("copies:"+r.getInt("copias")+" ");
			if (precio!=false) sb.append("precio:"+r.getFloat("precio")+" ");
			return sb.toString();
		}
		
	//METODOS DE LIBROS
		public static final String vistaLibros = "select * from libros order by titulo ASC  ";
		public void catalogoLibros(){
			System.out.println("IMPRIMIENDO CAFES . . .");
			this.s=null;this.r=null;this.conJava=null;
			try {
				conJava = pool.getConnection();
				s = conJava.createStatement();
				r = s.executeQuery(vistaLibros);
				while (r.next()){
					System.out.println(stringResultado(r,true,true,true,true,true,true,true));
				}
			} catch (SQLException e) {
				ConexionPool.printSQLException(e);
			}finally {
				cerrarR();
			}
		}

		public void verCatalogoInverso() throws Error{
			System.out.println("IMPRIMIENDO CAFES ORDEN INVERSO . . .");this.s=null;this.r=null;this.conJava=null;
			try {
				conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				r.last();
				do{
					System.out.println(stringResultado(r,true,true,true,true,true,true,false));
				}while(r.previous());
			} catch (SQLException e) {
				ConexionPool.printSQLException(e);
				throw new Error("Error en la BD");
			} finally {
				cerrarR();
			}
		}
		// METODO MODIFICADO PARA EL EJERCICIO 8
		public static final String anadirLibro = "insert into libros values (?,?,?,?,?,?,?)";
		public void agregarLibro(int isbn,String titulo, String autor,
			String editorial,int paginas,int copias) throws Error{
			System.out.println("AÑADIENDO LIBRO . . . ");this.p = null;this.conJava=null;
			try {
				conJava = pool.getConnection();
				p = conJava.prepareStatement(anadirLibro);
				p.setInt(1, isbn);p.setString(2, titulo);p.setString(3, autor);p.setString(4, editorial);
				p.setInt(5, paginas);p.setInt(6, copias);p.setNull(7, java.sql.Types.INTEGER);
				p.executeUpdate();
			} catch (SQLException e) { 
				ConexionPool.printSQLException(e); 
				throw new Error ("Ocurrio un error al acceder a los datos");
			} finally{ 
				cerrarR();
			}
		}
		
		public static final String borrarLibro= "delete from libros where isbn=? ";
		public void borrarLibro(int isbn) throws Error{
			System.out.println("BORRANDO LIBRO . . .");p=null;this.conJava=null;
			try {
				conJava = pool.getConnection();
				p = (PreparedStatement) conJava.prepareStatement(borrarLibro);
				p.setInt(1, isbn);
				p.executeUpdate();
			} catch (SQLException e) {
				ConexionPool.printSQLException(e);
				throw new Error("Ocurrio un error en la base de datos");
			}finally{
				cerrarR();
			}
		}
		public static final String existeLibro = "select isbn from libros where isbn=?";
		public static final String actualizarLibro= "update libros set copias = ? where isbn=?";
		public void actualizarCopia(int isbn, int copias) throws Error{
			System.out.println("ACTUALIZANDO UNA SOLA COPIA . . ."); s = null; r = null;this.conJava=null;
			try {
				conJava = pool.getConnection();
				PreparedStatement ps =  conJava.prepareStatement(existeLibro);
				ps.setInt(1, isbn);
				ResultSet rset = ps.executeQuery();
				if(rset.last()){
					p = (PreparedStatement) conJava.prepareStatement(actualizarLibro);
					p.setInt(1, copias);p.setInt(2, isbn);
					p.executeUpdate();
				}else{
					System.out.println("Isbn inexistente: "+isbn);
				}
			} catch (SQLException e) {
				ConexionPool.printSQLException(e);
				throw new Error("Ocurrio un error en la base de datos");
			}finally{
				cerrarR();
			}
		}
		
		//PROBANDO EL METODO DADO
		private static final String SELECT_CAMPOS_QUERY = "SELECT * FROM LIBROS LIMIT 1";
		public String[] getCamposLibro() throws Error {
		        p = null;r= null;this.conJava=null;
		        ResultSetMetaData rsmd = null;
		        String[] campos = null;
		        try {
		        	conJava = pool.getConnection();
		            p = conJava.prepareStatement(SELECT_CAMPOS_QUERY);
		            r = p.executeQuery();
		            rsmd = r.getMetaData();
		            int columns = rsmd.getColumnCount();
		            campos = new String[columns];
		            for (int i = 0; i < columns; i++) {
		                //Los indices de las columnas comienzan en 1
		                campos[i] = rsmd.getColumnLabel(i + 1);
		            }
		            return campos;
		        } catch (SQLException sqle) {
					ConexionPool.printSQLException(sqle);
					throw new Error("Ocurrió un error al acceder a los datos");
				} finally{
					cerrarR();
				}
			}
		
		public void actualizarCopias(HashMap<Integer , Integer> actualizacionCopy) throws Error{
			System.out.println("ACTUALIZANDO VARIAS COPIAS . . .");s = null;r= null;this.conJava=null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
	            while(r.next()){
	            	for (Map.Entry<Integer, Integer> e : actualizacionCopy.entrySet()){
	            		if (r.getInt("isbn")==e.getKey()){
	            			r.updateInt("copias", e.getValue().intValue() +r.getInt("copias"));
	            			r.updateRow();
	            		}
	            	} 
	            }
	            r.beforeFirst();
	        } catch (SQLException sqle) {
				// En una aplicación real, escribo en el log y delego
				ConexionPool.printSQLException(sqle);
				throw new Error("Ocurrió un error al acceder a los datos");
			} finally{
				cerrarR();
			}
		}

		public void mostrarFilas(ArrayList<Integer> numeroFila) throws Error{
			System.out.println("MOSTRANDO LAS FILAS ADECUADAS . . .");s = null;r= null;this.conJava=null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				r.beforeFirst();
	            for(int i = 0; i < numeroFila.size();i++){
	            	r.absolute(numeroFila.get(i));
	            	System.out.println(stringResultado(r,true,true,true,true,true,true,false));
	            }
	            r.beforeFirst();
	        } catch (SQLException sqle) {
				ConexionPool.printSQLException(sqle);
				throw new Error(	"Ocurrió un error al acceder a los datos");
			} finally{
				cerrarR();
			}
		}
		
		public void precioPagina() throws Error{
			System.out.println("CALCULANDO PRECIO DE LIBRO . . .");s = null;r= null;this.conJava=null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				while(r.next()){
					System.out.println("¿Que precio tiene por pagina este libro?");
	            	System.out.println(stringResultado(r, false, true, false, false, true, false,false));
	            	r.updateFloat("precio", (Float.parseFloat((new Scanner (System.in)).nextLine())*r.getInt("paginas")) );
	                r.updateRow();
	            }
	            r.beforeFirst();
	        } catch (SQLException sqle) {
				// En una aplicación real, escribo en el log y delego
				ConexionPool.printSQLException(sqle);
				throw new Error("Ocurrió un error al acceder a los datos");
			}catch (Exception e){ 
				throw new Error("No introdujo el formato correcto");
			} finally{
				cerrarR();
			}
		}
		
		
		public void transaccionPrecio (int isbn1, int isbn2, float precioPag) throws Error{
			System.out.println("CONSULTANDO LAS PAGINAS DE LOS DOS LIBROS . . . ");
			this.conJava=null;s = null;r= null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				float precioGuardado = 0.0f; int contadorAbsoluto=0;int pos1=0,pos2=0;
				while(r.next()){
					contadorAbsoluto++;
					int isbn = r.getInt("isbn");
					if (isbn1==isbn || isbn2==isbn){
						if(precioGuardado==0.0f)pos1=contadorAbsoluto; else pos2=contadorAbsoluto;
						System.out.println("LIBRO: "+stringResultado(r, true, true, false, false, true, false, false));
						System.out.print("  ==> El precio calculado da: "+r.getInt("paginas")*precioPag+"\n");
						if(precioGuardado<(r.getInt("paginas")*precioPag))precioGuardado=(r.getInt("paginas")*precioPag);
					}
	            }
				conJava.setAutoCommit(false);
				r.absolute(pos1);r.updateFloat("precio", precioGuardado);
	            r.updateRow();
	            r.absolute(pos2);r.updateFloat("precio", precioGuardado);
	            r.updateRow();
	            r.beforeFirst();
	            conJava.commit();
	        } catch (SQLException sqle) {
				ConexionPool.printSQLException(sqle);
				if (conJava != null) {
					try {
						System.err.println("Roll back de la transaccion");
						conJava.rollback();
					} catch (SQLException excep) {
						ConexionPool.printSQLException(sqle);
					}
					throw new java.lang.Error(
							"Ocurrio un error al acceder a los datos");
					}
			} finally{
				try {
					conJava.setAutoCommit(true);
				} catch (SQLException e) {
					ConexionPool.printSQLException(e);
				}
				cerrarR();
			}
		}
		
		public void anadirPaginas (int isbn, int paginas, float precioPag){
			System.out.println("OPERACION AÑADIR PAGINAS . . . ");
			this.conJava=null;s = null;r= null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				conJava.setAutoCommit(false);
				while(r.next()){
					int isbnOrig = r.getInt("isbn");
					if (isbnOrig==isbn ){
						r.updateInt("paginas", (r.getInt("paginas")+paginas));
						r.updateRow();
						r.updateFloat("precio", (r.getFloat("precio")+precioPag));
						r.updateRow();
						break;
					}
	            }
				r.beforeFirst();
	            conJava.commit();
	        } catch (SQLException sqle) {
				ConexionPool.printSQLException(sqle);
				if (conJava != null) {
					try {
						System.err.println("Roll back de la transaccion");
						conJava.rollback();
					} catch (SQLException excep) {
						ConexionPool.printSQLException(sqle);
					}
					throw new java.lang.Error("Ocurrio un error al acceder a los datos");
					}
			} finally{
				cerrarR();
				try {
					conJava.setAutoCommit(true);
				} catch (SQLException e) {
					ConexionPool.printSQLException(e);
				}
			}
		}
		
		public void duplicarLibro (int isbn1, int isbn2) throws Error{
			System.out.println("COPIANDO DATOS DEL PRIMER ISBN . . . ");
			java.sql.Statement s2=null;ResultSet r2 = null;this.conJava=null;s = null;r= null;
	        try {
	        	conJava = pool.getConnection();
				s= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				r = s.executeQuery(vistaLibros);
				s2= conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				 r2 = s2.executeQuery(vistaLibros);
				
				while(r.next()){
					int isbnOrig = r.getInt("isbn");
					if (isbnOrig==isbn1 ){
						System.out.println("Copiando este: "+stringResultado(r, true, true, true, true, true, true, true)+" a ==>");
						r2.moveToInsertRow();
						r2.updateInt("isbn",isbn2);
						r2.updateString("titulo",r.getString("titulo"));
						r2.updateString("autor", r.getString("autor"));r2.updateString("editorial", r.getString("editorial"));
						r2.updateInt("paginas",r.getInt("paginas"));r2.updateInt("copias",r.getInt("copias"));
						r2.updateFloat("precio",r.getFloat("precio"));
						r2.insertRow();
						// autor editorial paginas copias precio
					}
	            }
				System.out.println(stringResultado(r2, true, true, true, true, true, true, true));
				r.beforeFirst();r2.beforeFirst();
				
	        } catch (SQLException sqle) {
				ConexionPool.printSQLException(sqle);
				throw new java.lang.Error("Ocurrio un error al acceder a los datos");
			} finally{
				try {
					s2.close();
				} catch (SQLException e) {
					ConexionPool.printSQLException(e);
					throw new Error("Error conexion 2 ");
				}
				cerrarR();
			}
		}
}
