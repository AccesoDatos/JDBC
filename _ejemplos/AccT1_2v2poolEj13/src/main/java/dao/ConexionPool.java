package dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

public class ConexionPool {
	public String dbms;//mysql
	public String driver;
	public String servidor;
	public int puerto;
	public String bd;
	public String usuario;
	public String pwd;
	public String url;
	// ----------------- 1   CONSTRUCCION CONEX: ↑ PROP del XML   -----------------------------------------
	Properties p;
	public final String PROPERTIES_FILE = "src\\main\\resources\\propiedad.xml";
	public ConexionPool() throws InvalidPropertiesFormatException, IOException{
		System.out.println("LEyendo el Fichero de propiedades");
		this.setPropiedades(PROPERTIES_FILE);
	}
	public void setPropiedades(String fichero) throws InvalidPropertiesFormatException, IOException{
		this.p = new Properties();
		p.loadFromXML(Files.newInputStream(Paths.get(fichero)));
		this.dbms=this.p.getProperty("dbms");
		this.driver=this.p.getProperty("driver");
		this.servidor=this.p.getProperty("servidor");
		this.puerto=Integer.parseInt(this.p.getProperty("puerto"));
		this.bd=this.p.getProperty("bd");
		this.usuario=this.p.getProperty("usuario");
		this.pwd=this.p.getProperty("pwd");
		System.out.printf("Las propiedades definidas para nuestra clase Conexion:%n ManegerDB:%s,   "
		+ "Driver:%s,  %n Servidor:%s | Puerto:%d %n BaseDatos:%s,    Usuario:%s    y la Contraseña:%s"
		+ "%n%n",dbms,driver,servidor,puerto,bd,usuario,pwd);
	}
	// Aunque no te lance la excepcion yo se la meto
	public BasicDataSource getPool() throws SQLException{
		BasicDataSource bDs = new BasicDataSource();
		bDs.setDriverClassName(driver);
		
		bDs.setUsername(this.usuario);
		bDs.setPassword(this.pwd);
		bDs.setUrl("jdbc:"+this.dbms+"://"+this.servidor+":"+this.puerto+"/"+this.bd);
		bDs.setInitialSize(4);
		bDs.setValidationQuery("select 1");
		System.out.println("Pool se ha creado correctamente");
		return bDs;
	}
	
	public static void cerrarPool(BasicDataSource pool){
		System.out.println("Liberando todos los recursos abientos . . . ");
		try {
			if(pool!=null){
			pool.close();
			pool = null;
			}
		} catch (SQLException e) {
			System.err.println(e);
		}
	}
	
	public static void printSQLException(SQLException sqlE) {
        while (sqlE != null) {
			if (sqlE instanceof SQLException) {
				sqlE.printStackTrace(System.err);
				System.err.println("Estado del SQL: "+sqlE.getSQLState());
				System.err.println("CODIGO Error - propio de cada gestor de BD - SQL: "+sqlE.getErrorCode());
				System.err.println("Mensaje textual del gestor SQL: "+sqlE.getMessage());
				//Objetos desencadenantes de la excepcion
				Throwable t = sqlE.getCause();
				while (t != null) {
					System.out.println("La causa de los errores: " + t);
					t = t.getCause();
				}
			}//Cualquier otra excepcion encadenada
			sqlE = sqlE.getNextException();	
        }
	}
	
}
