package mySql;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class Conexion{
	public String dbms;//database management system -> mysql
	public String driver;// driver usado -> 
	public String servidor;
	public int puerto;//*-
	public String bd;
	public String usuario;
	public String pwd;
	
	// ----------------- 1   CONSTRUCCION CONEX: ↑ PROP del XML   -----------------------------------------
	Properties p;
	public static final String propiedadesXMLurl = "src/main/resources/resources/propiedades_conn.xml";
	//Quito el argumento de entrada, no vamos a pasarle ARGS[0]
	public Conexion() throws InvalidPropertiesFormatException, IOException{
		System.out.println("Leyendo el fichero de propiedades . . .");
		this.setPropiedades(propiedadesXMLurl);
	}
	
	private void setPropiedades(String propiedadesXMLurl) throws InvalidPropertiesFormatException, IOException{
		this.p=new Properties();
		p.loadFromXML(Files.newInputStream(Paths.get(propiedadesXMLurl)));
		this.dbms=this.p.getProperty("dbms");
		this.driver=this.p.getProperty("driver");
		this.servidor=this.p.getProperty("servidor");
		this.puerto=Integer.parseInt(this.p.getProperty("puerto"));
		this.bd=this.p.getProperty("bd");
		this.usuario=this.p.getProperty("usuario");
		this.pwd=this.p.getProperty("pwd");
		System.out.printf("Las propiedades definidas para nuestra clase Conexion:%n ManegerDB:%s,   "
		+ "Driver:%s,  %n Servidor:%s | Puerto:%d %n BaseDatos:%s,    Usuario:%s    y la Contraseña:%s"
		+ "%n%n",dbms,driver,servidor,puerto,bd,usuario,pwd);
	}
	

	// ----------------- 2   CONX = CLASE.PROP A CONEX  -----------------------------------------
	public Connection getConexion() {
		Connection con=null;
		Properties pconex = new Properties();
		pconex.put("user",this.usuario);pconex.put("password", this.pwd);
		if (this.dbms.equals("mysql")){
			String url ="jdbc:"+this.dbms+"://"+this.servidor+":"+this.puerto+"/"+this.bd; 
			try {
				con = DriverManager.getConnection(url,pconex);
			} catch (SQLException e) {
				Conexion.printSQLException(e);
			}// es una conexion cualquiera. Tratamos la excepcion para liberar codigo
			//Evitas la repeticion, siempre tratas una excepcion SQL
		}
		System.out.println("Se ha podido conectar a la Base de Datos !!!!! \n");
	 return con;
	}
	public static void exitConexion (Connection conexionexterna) {
		System.out.println("\n Liberando todos los recursos . . .");
		try {conexionexterna.close();
		} catch (SQLException e) {e.printStackTrace();}
		conexionexterna=null;
	}// es una conexion cualquiera. Tratamos la excepcion para liberar codigo
	
	public static void printSQLException(SQLException sqlE) {
        while (sqlE != null) {
			if (sqlE instanceof SQLException) {
				sqlE.printStackTrace(System.err);
				System.err.println("Estado del SQL: "+sqlE.getSQLState());
				System.err.println("CODIGO Error - propio de cada gestor de BD - SQL: "+sqlE.getErrorCode());
				System.err.println("Mensaje textual del gestor SQL: "+sqlE.getMessage());
				//Objetos desencadenantes de la excepcion
				Throwable t = sqlE.getCause();
				while (t != null) {
					System.out.println("La causa de los errores: " + t);
					t = t.getCause();
				}
			}//Cualquier otra excepcion encadenada
			sqlE = sqlE.getNextException();	
        }}}

