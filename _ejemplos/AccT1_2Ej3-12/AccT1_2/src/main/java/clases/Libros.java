package clases;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Scanner;

import conexion.Conexion;
import error.Error;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.Statement;

public class Libros {
	private Connection conJava;
	private Statement s;
	private PreparedStatement p;
	private ResultSet r;
	//Crear la conexion con nuestra clase conexion para volcar las propiedades
	public Libros() {
		try { // Declaras un objeto conexion al que llama a las clases de conection para volcarle los valores personalizados
			Conexion c= new Conexion();
			this.conJava = (Connection) c.getConexion();
			s=null;p=null;
		} catch (InvalidPropertiesFormatException e) {e.printStackTrace();
		} catch (IOException e) {
		}
		
	}
	//Terminar la conexiones con BD y liberar recursos
	public void cerrarConexion(){Conexion.exitConexion(conJava);}
	public void  cerrarRecursos(){
		try {
			if(s!=null)s.close();
			if(p!=null)p.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
//METODO GENERAL - MODIFICADO PARA EL EJERCICIO 8
	public String stringResultado(ResultSet r,boolean isbn,boolean titulo,boolean autor,
			boolean editorial,boolean paginas,boolean copias, boolean precio ) throws SQLException{
		StringBuffer sb = new StringBuffer ("");
		if (isbn!=false) sb.append("isbn:"+r.getInt("isbn")+" ");
		if (titulo!=false) sb.append("title:"+r.getString("titulo")+" ");
		if (autor!=false) sb.append("autor:"+r.getString("autor")+" ");
		if (editorial!=false) sb.append("editorial:"+r.getString("editorial")+" ");
		if (paginas!=false) sb.append("pages:"+r.getInt("paginas")+" ");
		if (copias!=false) sb.append("copies:"+r.getInt("copias")+" ");
		if (precio!=false) sb.append("precio:"+r.getFloat("precio")+" ");
		return sb.toString();
	}
	
//METODOS DE LIBROS
	public static final String vistaLibros = "select * from libros order by titulo ASC  ";
	public void catalogoLibros(){
		System.out.println("IMPRIMIENDO CAFES . . .");this.s=null;this.r=null;
		try {
			s = (Statement) conJava.createStatement();
			r = s.executeQuery(vistaLibros);
			while (r.next()){
				System.out.println(stringResultado(r,true,true,true,true,true,true,true));
			}
		} catch (SQLException e) {
			Conexion.printSQLException(e);
		}finally {
			cerrarRecursos();
		}
	}

	public void verCatalogoInverso() throws Error{
		System.out.println("IMPRIMIENDO CAFES ORDEN INVERSO . . .");this.s=null;this.r=null;
		try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			r.last();
			//Debo hacerlo de esta manera porque sino sube uno arriba y eso es lo que no queremos
			// que se salte informacion
			do{
				System.out.println(stringResultado(r,true,true,true,true,true,true,false));
			}while(r.previous());
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error("Error en la BD");
		} finally {
			cerrarRecursos();
		}
	}
	// METODO MODIFICADO PARA EL EJERCICIO 8
	public static final String anadirLibro = "insert into libros values (?,?,?,?,?,?,?)";
	public void agregarLibro(int isbn,String titulo, String autor,
			String editorial,int paginas,int copias) throws Error{
		System.out.println("AÑADIENDO LIBRO . . . ");this.p = null;
		try {
			p = (PreparedStatement) conJava.prepareStatement(anadirLibro);
			p.setInt(1, isbn);p.setString(2, titulo);p.setString(3, autor);p.setString(4, editorial);
			p.setInt(5, paginas);p.setInt(6, copias);p.setNull(7, java.sql.Types.INTEGER);
			p.executeUpdate();
		} catch (SQLException e) { 
			Conexion.printSQLException(e); 
			throw new Error ("Ocurrio un error al acceder a los datos");
		} finally{ 
			cerrarRecursos();
		}
	}
	
	public static final String borrarLibro= "delete from libros where isbn=? ";
	public void borrarLibro(int isbn) throws Error{
		System.out.println("BORRANDO LIBRO . . .");p=null;
		try {
			p = (PreparedStatement) conJava.prepareStatement(borrarLibro);
			p.setInt(1, isbn);
			p.executeUpdate();
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error("Ocurrio un error en la base de datos");
		}finally{
			cerrarRecursos();
		}
	}
	
	public static final String actualizarLibro= "update libros set copias = ? where isbn=?";
	public void actualizarCopia(int isbn, int copias) throws Error{
		System.out.println("ACTUALIZANDO UNA SOLA COPIA . . ."); s = null; r = null;
		try {
			p = (PreparedStatement) conJava.prepareStatement(actualizarLibro);
			p.setInt(1, copias);p.setInt(2, isbn);
			p.executeUpdate();
		} catch (SQLException e) {
			Conexion.printSQLException(e);
			throw new Error("Ocurrio un error en la base de datos");
		}finally{
			cerrarRecursos();
		}
	}
	
	//PROBANDO EL METODO DADO
	private static final String SELECT_CAMPOS_QUERY = "SELECT * FROM LIBROS LIMIT 1";
	public String[] getCamposLibro() throws Error {
	        p = null;r= null;
	        ResultSetMetaData rsmd = null;
	        String[] campos = null;
	        try {
	            //Solicitamos a la conexion un objeto stmt para nuestra consulta
	            p = (PreparedStatement) conJava.prepareStatement(SELECT_CAMPOS_QUERY);
	            //Le solicitamos al objeto stmt que ejecute nuestra consulta
	            //y nos devuelve los resultados en un objeto ResultSet
	            r = p.executeQuery();
	            rsmd = (ResultSetMetaData) r.getMetaData();
	            int columns = rsmd.getColumnCount();
	            campos = new String[columns];
	            for (int i = 0; i < columns; i++) {
	                //Los indices de las columnas comienzan en 1
	                campos[i] = rsmd.getColumnLabel(i + 1);
	            }
	            return campos;
	        } catch (SQLException sqle) {
				// En una aplicación real, escribo en el log y delego
				Conexion.printSQLException(sqle);
				throw new Error(	"Ocurrió un error al acceder a los datos");
			} finally{
				cerrarRecursos();
			}
		}
	// HasMap isbn copia
	public void actualizarCopias(HashMap<Integer , Integer> actualizacionCopy) throws Error{
		System.out.println("ACTUALIZANDO VARIAS COPIAS . . .");s = null;r= null;
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
            while(r.next()){
            	//Por cada columna recibida comprobamos la igualdad, hacemos update y subimos
            	for (Map.Entry<Integer, Integer> e : actualizacionCopy.entrySet()){
            		if (r.getInt("isbn")==e.getKey()){
            			r.updateInt("copias", e.getValue().intValue() +r.getInt("copias"));
            			r.updateRow();
            		}
            	} 
            }
            r.beforeFirst();
        } catch (SQLException sqle) {
			// En una aplicación real, escribo en el log y delego
			Conexion.printSQLException(sqle);
			throw new Error(	"Ocurrió un error al acceder a los datos");
		} finally{
			cerrarRecursos();
		}
	}

	public void mostrarFilas(ArrayList<Integer> numeroFila) throws Error{
		System.out.println("MOSTRANDO LAS FILAS ADECUADAS . . .");s = null;r= null;
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			r.beforeFirst();
            for(int i = 0; i < numeroFila.size();i++){
            	r.absolute(numeroFila.get(i));
            	System.out.println(stringResultado(r,true,true,true,true,true,true,false));
            }
            r.beforeFirst();
        } catch (SQLException sqle) {
			// En una aplicación real, escribo en el log y delego
			Conexion.printSQLException(sqle);
			throw new Error(	"Ocurrió un error al acceder a los datos");
		} finally{
			cerrarRecursos();
		}
	}
	
	public void precioPagina() throws Error{
		System.out.println("CALCULANDO PRECIO DE LIBRO . . .");s = null;r= null;
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			while(r.next()){
				System.out.println("¿Que precio tiene por pagina este libro?");
            	System.out.println(stringResultado(r, false, true, false, false, true, false,false));
            	r.updateFloat("precio", (Float.parseFloat((new Scanner (System.in)).nextLine())*r.getInt("paginas")) );
                r.updateRow();
            }
            r.beforeFirst();
        } catch (SQLException sqle) {
			// En una aplicación real, escribo en el log y delego
			Conexion.printSQLException(sqle);
			throw new Error(	"Ocurrió un error al acceder a los datos");
		}catch (Exception e){ 
			throw new Error(	"No introdujo el formato correcto");
		} finally{
			cerrarRecursos();
		}
	}
	
	
	public void transaccionPrecio (int isbn1, int isbn2, float precioPag) throws Error{
		System.out.println("CONSULTANDO LAS PAGINAS DE LOS DOS LIBROS . . . ");
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			float precioGuardado = 0.0f; int contadorAbsoluto=0;int pos1=0,pos2=0;
			while(r.next()){
				contadorAbsoluto++;
				int isbn = r.getInt("isbn");
				if (isbn1==isbn || isbn2==isbn){
					//No empezo a guardar precio luego identifico el primer isbn igual
					//En el momento que calcula el precio del primera ya entra en el else
					if(precioGuardado==0.0f)pos1=contadorAbsoluto; else pos2=contadorAbsoluto;
					System.out.println("LIBRO: "+stringResultado(r, true, true, false, false, true, false, false));
					System.out.print("  ==> El precio calculado da: "+r.getInt("paginas")*precioPag+"\n");
					if(precioGuardado<(r.getInt("paginas")*precioPag))precioGuardado=(r.getInt("paginas")*precioPag);
				}
            }
			conJava.setAutoCommit(false);
			//Realizando transaccion
            r.absolute(pos1);r.updateFloat("precio", precioGuardado);
            r.updateRow();
            r.absolute(pos2);r.updateFloat("precio", precioGuardado);
            r.updateRow();
            r.beforeFirst();
            conJava.commit();
        } catch (SQLException sqle) {
			// En una aplicación real, escribo en el log y delego
			Conexion.printSQLException(sqle);
			if (conJava != null) {
				try {//Si falla todos los datos de ps se deshacen
					System.err.println("Roll back de la transaccion");
					conJava.rollback();
				} catch (SQLException excep) {
					Conexion.printSQLException(sqle);
				}
				throw new java.lang.Error(
						"Ocurrio un error al acceder a los datos");
				}
		} finally{
			try {
				conJava.setAutoCommit(true);
			} catch (SQLException e) {
				Conexion.printSQLException(e);
			}
			cerrarRecursos();
		}
	}
	
	public void anadirPaginas (int isbn, int paginas, float precioPag){
		System.out.println("OPERACION AÑADIR PAGINAS . . . ");
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			conJava.setAutoCommit(false);
			while(r.next()){
				int isbnOrig = r.getInt("isbn");
				if (isbnOrig==isbn ){
					r.updateInt("paginas", (r.getInt("paginas")+paginas));
					r.updateRow();
					r.updateFloat("precio", (r.getFloat("precio")+precioPag));
					r.updateRow();
					break;
				}
            }
			r.beforeFirst();
            conJava.commit();
        } catch (SQLException sqle) {
			Conexion.printSQLException(sqle);
			if (conJava != null) {
				try {
					System.err.println("Roll back de la transaccion");
					conJava.rollback();
				} catch (SQLException excep) {
					Conexion.printSQLException(sqle);
				}
				throw new java.lang.Error("Ocurrio un error al acceder a los datos");
				}
		} finally{
			try {
				conJava.setAutoCommit(true);
			} catch (SQLException e) {
				Conexion.printSQLException(e);
			}
			cerrarRecursos();
		}
	}
	
	public void duplicarLibro (int isbn1, int isbn2) throws Error{
		System.out.println("COPIANDO DATOS DEL PRIMER ISBN . . . ");Statement s2=null;ResultSet r2 = null;
        try {
			s= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			r = s.executeQuery(vistaLibros);
			s2= (Statement) conJava.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			 r2 = s2.executeQuery(vistaLibros);
			
			while(r.next()){
				int isbnOrig = r.getInt("isbn");
				if (isbnOrig==isbn1 ){
					System.out.println("Copiando este: "+stringResultado(r, true, true, true, true, true, true, true)+" a ==>");
					r2.moveToInsertRow();
					r2.updateInt("isbn",isbn2);
					r2.updateString("titulo",r.getString("titulo"));
					r2.updateString("autor", r.getString("autor"));r2.updateString("editorial", r.getString("editorial"));
					r2.updateInt("paginas",r.getInt("paginas"));r2.updateInt("copias",r.getInt("copias"));
					r2.updateFloat("precio",r.getFloat("precio"));
					r2.insertRow();
					// autor editorial paginas copias precio
				}
            }
			System.out.println(stringResultado(r2, true, true, true, true, true, true, true));
			r.beforeFirst();r2.beforeFirst();
			
        } catch (SQLException sqle) {
			Conexion.printSQLException(sqle);
			throw new java.lang.Error("Ocurrio un error al acceder a los datos");
		} finally{
			try {
				s2.close();
			} catch (SQLException e) {
				Conexion.printSQLException(e);
				throw new Error("Error conexion 2 ");
			}
			cerrarRecursos();
		}
	}
}
