package clases;

public class Sentencias {
	public static final String SELECT_CAFE = "select CAF_NOMBRE, PROV_ID, PRECIO, VENTAS, TOTAL from CAFES";
	public static final String SEARCH_CAFE = "select * from CAFES WHERE CAF_NOMBRE= ?";
	public static final String INSERT_CAFE = "insert into CAFES values (?,?,?,?,?)";
	public static final String DELETE_CAFE = "delete from CAFES WHERE CAF_NOMBRE = ?";
	public static final String UPDATE_CAFE_ventas = "update CAFES set VENTAS = ? where CAF_NOMBRE = ?";
	public static final String UPDATE_CAFE_total = "update CAFES set TOTAL = TOTAL + ? where CAF_NOMBRE = ?";

	//EJERCICIO 1
	public static final String SEARCH_CAFE_prv = "select c.caf_nombre,c.precio,c.ventas, c.total,p.PROV_ID, p.CALLE, p.CIUDAD, p.PAIS, p.CP  from cafes c inner join proveedores p  on c.PROV_ID=p.PROV_ID where c.PROV_ID=?;";
}
