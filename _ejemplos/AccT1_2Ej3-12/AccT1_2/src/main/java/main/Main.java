package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import clases.Libros;
import error.Error;
public class Main {
    public static void main( String[] args ) {
        Scanner sc = new Scanner(System.in);
        boolean exit=false;
    	int opc = 0;
    	Libros l = new Libros();
    	
    	while(!exit){
        	System.out.printf( "\n----------------------------------------------------"
        			+ "\n¿Que desea hacer: \n 1 ver catalogo  2 ver catalogo inverso 3 subir un libro "
        			+ "4 borrar libro \n  5 actualizar una copia 6 probar el metodo 7 actualizar varias copias"
        			+ " \n8 listadoConNumeros 9 Precio por pagina - Calcular precio"
        			+ " 10  Transaccion de libros 11 añadir paginas"
        			+ "\n 12 duplicar libro 13 salir" );
        	opc = Integer.parseInt(sc.nextLine());
        	try {
	        	switch (opc){
		        	case 1:	l.catalogoLibros();
		        		break;
		        	case 2:	l.verCatalogoInverso();
		        		break;
		        	case 3:	l.agregarLibro(1200, "Programacion avanzada", "Oscar Manuel", "Piramides", 150, 300);
						break;
		        	case 4:	l.borrarLibro(1200);
		        		break;
		        	case 5:	l.actualizarCopia(1200, 200);
		        		break;
		        	case 6:	String[] a =l.getCamposLibro();
						        		for (int i = 0; i < a.length; i++) {
											System.out.println(a[i]);
										}
		        		break;
		        	case 7:
		        		HashMap<Integer, Integer> copias = new HashMap<Integer, Integer>();
		        		copias.put(1325, 3);
		        		copias.put(12453, 4);
		        		l.actualizarCopias(copias);
		        		break;
		        	case 8: 	
		        		ArrayList<Integer> listadoNumeros = new ArrayList<Integer>();
		        		listadoNumeros.add(new Integer(2));
		        		listadoNumeros.add(new Integer(1));
		        		l.mostrarFilas(listadoNumeros);
		        		break;
		        	case 9:	
		        		l.precioPagina();
		        		break;
		        	case 10:
		        		l.transaccionPrecio(1725, 1325, 0.2f);
		        		break;
		        	case 11:
		        		l.anadirPaginas(12345, 15, 0.1f);
		        		break;
		        	case 12:
		        		l.duplicarLibro(1725, 2201);
		        		break;
		        	case 13:	
		        		exit=true;l.cerrarConexion();
		        		break;
		        }
        	} catch (Error e) {System.err.println(e);}
        }
    	sc.close();
    }
}
